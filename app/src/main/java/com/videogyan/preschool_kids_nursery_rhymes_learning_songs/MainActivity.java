package com.videogyan.preschool_kids_nursery_rhymes_learning_songs;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.appodeal.ads.Appodeal;
import com.appodeal.ads.InterstitialCallbacks;

public class MainActivity extends AppCompatActivity {

    final String TAG = "Appodeal";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Appodeal.setTesting(true);
        Appodeal.setLogLevel(com.appodeal.ads.utils.Log.LogLevel.verbose);
        Appodeal.initialize(this, getString(R.string.appodeal_id), Appodeal.INTERSTITIAL);
        Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
            @Override
            public void onInterstitialLoaded(boolean isPrecache) {
                Log.d(TAG, "AppodealAd::onInterstitialLoaded");
            }
            @Override
            public void onInterstitialFailedToLoad() {
                Log.d(TAG, "AppodealAd::onInterstitialFailedToLoad");
            }
            @Override
            public void onInterstitialShown() {
                Log.d(TAG, "AppodealAd::onInterstitialShown");
            }
            @Override
            public void onInterstitialClicked() {
                Log.d(TAG, "AppodealAd::onInterstitialClicked");
            }
            @Override
            public void onInterstitialClosed() {
                Log.d(TAG, "AppodealAd::onInterstitialClosed");
//                adListener.onAdClosed();
            }
            @Override
            public void onInterstitialExpired() {
                Log.d(TAG, "AppodealAd::onInterstitialExpired");
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (event.getKeyCode()) {
                case KeyEvent.KEYCODE_0:
                    showAd();
                    break;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    void showAd() {
        if(Appodeal.isLoaded(Appodeal.INTERSTITIAL)) {
            Log.d(TAG, "showAd: interstitial is loaded.");
            Appodeal.show(this, Appodeal.INTERSTITIAL);
        } else {
            Log.d(TAG, "showAd: interstitial is not loaded.");
        }
    }
}
